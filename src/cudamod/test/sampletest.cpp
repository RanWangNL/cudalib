//
// Created by user on 8-6-16.
//


#include <gtest/gtest.h>
#include <memory>
#include <iostream>
#include "../include/codamod/CudaMatrix.cuh"
#include "../include/codamod/CudaRandom.cuh"
class CudaMatrix_Test : public ::testing::TestWithParam<int> {
public:
    CudaMatrix_<double> mydouble;
    CudaNormalRandom_<double> doubleRandom;
    CudaMatrix_Test(): mydouble(10,10),doubleRandom(0,0.0,1.0){
        std::cout << "Hei, I am setting up the test" << std::endl;
    }

    //void SetUp( ) {
    // code here will execute just before the test ensues
    //}


    //void TearDown( ) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    //}

    //~myTestFixture1( )  {
    // cleanup any pending stuff, but no exceptions allowed
    //}

    // put in any custom data members that you need

};
TEST_F(CudaMatrix_Test, TestSetDevice){
    CudaMatrixhelper_<double>::setDeviceMatrix(mydouble,0.0);
    mydouble.syncDeviceToHost();
//    mydouble.printMatrix();
    ASSERT_EQ(1,1);
}
TEST_F(CudaMatrix_Test, TestGetter){
    double b = CudaMatrixhelper_<double>::get(mydouble, 0,0);
    ASSERT_EQ(b,0.0);
}

TEST_F(CudaMatrix_Test, TestPrint){
//    mydouble.printMatrix();
    ASSERT_EQ(1,1);
}
TEST_F(CudaMatrix_Test, TestPassByPointer){
test_function();
}
TEST_F(CudaMatrix_Test, TestRandom){
//    doubleRandom.generate(mydouble);
//    mydouble.printMatrix();
//    mydouble.syncDeviceToHost();
//    mydouble.printMatrix();
    ASSERT_EQ(1,1);
}
INSTANTIATE_TEST_CASE_P(Instantiation, CudaMatrix_Test, ::testing::Range(1, 10));