//
// Created by user on 9-6-16.
//
#ifndef CUDATRIAL_CUDAMATRIX_H
#define CUDATRIAL_CUDAMATRIX_H

#include <cassert>
#include "Macro.cuh"
template<typename T>
class CudaMatrix_ {
private:
    int nRow;
    int nCol;

public:
    T* hostptr;
    T* devptr;


    CUDA_HOST CudaMatrix_(int nRow_, int nCol_);
    CUDA_HOST CudaMatrix_(T* data, int nRow_, int nCol_);

    CUDA_CALLABLE_MEMBER CudaMatrix_(const CudaMatrix_<T>& other)=delete ;
    CUDA_CALLABLE_MEMBER CudaMatrix_<T> operator=(const CudaMatrix_<T> & other)=delete;
    CUDA_HOST CudaMatrix_(CudaMatrix_<T>&& other)=delete;
    CUDA_HOST CudaMatrix_<T> operator=(CudaMatrix_<T>&& other) = delete;
//  Delete all these things so that there is no way that the same pointer is going to be deleted twice;
    CUDA_CALLABLE_MEMBER int getNRow();
    CUDA_CALLABLE_MEMBER int getNCol();
    CUDA_HOST void syncDeviceToHost();
    CUDA_HOST void syncHostToDevice();
    CUDA_HOST void printRow(const int);
    CUDA_HOST void printCol(const int);
    CUDA_HOST void printMatrix();
    CUDA_HOST ~CudaMatrix_();

    CUDA_CALLABLE_MEMBER void setDeviceConstant(const T number);
};
template<typename T>
class CudaMatrixhelper_ {
public:
    CUDA_HOST static void set(CudaMatrix_<T> &other, int i, int j, T value);
    CUDA_HOST static T get(CudaMatrix_<T> &other, int i, int j);
    CUDA_HOST static void setDeviceMatrix(CudaMatrix_<T>& other, const T value);
};

void test_function();
#endif //CUDATRIAL_CUDAMATRIX_H

