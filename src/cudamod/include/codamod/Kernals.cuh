//
// Created by user on 15-6-16.
//

#ifndef CUDATRIAL_KERNALS_H
#define CUDATRIAL_KERNALS_H

#include "cuda_runtime.h"
#include "CudaMatrix.cuh"
template<typename T>
__global__ void setDeviceConstant(T* data, const int Nrow, const int Ncol, const T number);


__global__ void TestKernel(CudaMatrix_<double>* T);

#endif //CUDATRIAL_KERNALS_H
