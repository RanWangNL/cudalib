//
// Created by user on 13-6-16.
//

#ifndef CUDATRIAL_CUDARANDOM_H
#define CUDATRIAL_CUDARANDOM_H
#include "CudaMatrix.cuh"
#include "Macro.cuh"
#include <boost/random.hpp>
template<typename T>
class CudaNormalRandom_ {
private:
    typedef boost::random::mt19937_64 generator;
    typedef boost::random::normal_distribution<>  distribution;
    typedef boost::variate_generator<generator&, distribution > gen;
    generator myg;
    distribution mydist;
    gen mygen;

    CUDA_HOST void create(gen, CudaMatrix_<T>& other);
public:

    CUDA_HOST CudaNormalRandom_(int seed, double mean, double variance);
    template<typename target>
    void generate(target& t){
        create(mygen, t);
    }

    template<typename target, typename ... Args>
    void generate(target& t, Args& ... args){
        generate(t);
        generate(args...);
    }
};



#endif //CUDATRIAL_CUDARANDOM_H
