//
// Created by user on 9-6-16.
//

#include "../include/codamod/CudaMatrix.cuh"
#include "../include/codamod/Macro.cuh"
#include <cstdio>
#include <cuda_runtime.h>
#include <iostream>
#include <device_launch_parameters.h>

template<typename T>
CUDA_HOST CudaMatrix_<T>::CudaMatrix_(int nRow_, int nCol_) {
    const unsigned int  size = sizeof(T)*nRow_*nCol_;
    nRow = nRow_; nCol = nCol_;
    cudaMallocHost((void**)&hostptr, size);
    cudaMalloc((void**)&devptr,size);

    for (int j=0; j != nCol;j++){
        for (int i = 0; i != nRow; i++){
            hostptr[i+j*nRow] = (T)0;
        }
    }
    cudaMemcpy(devptr, hostptr, size, cudaMemcpyHostToDevice);

}

template<typename T>
CUDA_HOST CudaMatrix_<T>::CudaMatrix_(T *data, int nRow_, int nCol_) {
    const unsigned int  size = sizeof(T)*nRow_*nCol_;
    nRow = nRow_; nCol = nCol_;
    cudaMallocHost((void**)&hostptr, size);
    cudaMalloc((void**)&devptr,size);

//    for (long int i=0;i!=nRow*nCol;++i){
//        hostptr[i]=data[i];
//    }
    memcpy(hostptr, data, sizeof(T)*nCol*nRow);

    cudaMemcpy(devptr, hostptr, size, cudaMemcpyHostToDevice);
}
//template<typename T>
//CUDA_CALLABLE_MEMBER CudaMatrix_<T>::CudaMatrix_(const CudaMatrix_<T> &other) {
//    this->nRow = other.nRow;
//    this->nCol = other.nCol;
//    this->devptr = other.devptr;
//    this->hostptr = other.hostptr;
//}
template<typename T>
CUDA_HOST CudaMatrix_<T>::~CudaMatrix_() {
    cudaFreeHost(hostptr);
    cudaFree(devptr);
}

template<typename T>
CUDA_CALLABLE_MEMBER int CudaMatrix_<T>::getNRow(){
    return this->nRow;
}



template<typename T>
CUDA_CALLABLE_MEMBER int CudaMatrix_<T>::getNCol(){
    return this->nCol;
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::syncDeviceToHost() {
    cudaMemcpy(hostptr, devptr,sizeof(T)*nRow*nCol, cudaMemcpyDeviceToHost);
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::syncHostToDevice() {
    cudaMemcpy(hostptr, devptr,sizeof(T)*nRow*nCol, cudaMemcpyHostToDevice);
}
template<typename T>
CUDA_HOST void CudaMatrixhelper_<T>::set(CudaMatrix_<T> &other, int i, int j, T value) {
    CHECK_BOUND(i, other.getNRow());
    CHECK_BOUND(i, other.getNCol());
    other.hostptr[i+other.getNRow()*j]=value;
}

template<typename T>
CUDA_HOST T CudaMatrixhelper_<T>::get(CudaMatrix_<T> &other, int i, int j) {
    CHECK_BOUND(i, other.getNRow());
    CHECK_BOUND(i, other.getNCol());
    return (other.hostptr[i+other.getNRow()*j]);
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::printRow(const int row) {
    assert(row < this->getNCol());
    std::cout << this->hostptr[row];
    const int nRow = this->getNRow();
    for (int i =1;i!=this->getNCol();++i){
        std::cout << "," << this->hostptr[row+nRow*i];
    }
    std::cout << std::endl;
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::printCol(const int col) {
    assert(col < this->getNRow());
    std::cout << this->hostptr[col];
    const int nRow = this->getNRow();
    for (int i =1;i!=this->getNRow();++i){
        std::cout << "," << this->hostptr[i+nRow*col];
    }
    std::cout << std::endl;
}

template<typename T>
CUDA_HOST void CudaMatrix_<T>::printMatrix() {
    for (int i = 0 ; i!=this->getNRow();++i){
        this->printRow(i);
    }

}
void forcedInitialize(){
    CudaMatrix_<double> doubleProxy(10,10);
    CudaMatrix_<int> intProxy(10,10);
    CudaMatrix_<float> floatProxy(10,10);
    auto  a = CudaMatrixhelper_<double>::get(doubleProxy,0,0);
    CudaMatrixhelper_<double>::set(doubleProxy,0,0,0.0);
    auto  b = CudaMatrixhelper_<float>::get(floatProxy,0,0);
    CudaMatrixhelper_<float>::set(floatProxy,0,0,0.0);
    auto c = CudaMatrixhelper_<int>::get(intProxy,0,0);
    CudaMatrixhelper_<int>::set(intProxy,0,0,0);
    CudaMatrixhelper_<double>::setDeviceMatrix(doubleProxy,1.0);
    CudaMatrixhelper_<float>::setDeviceMatrix(floatProxy,1.0);
    CudaMatrixhelper_<int>::setDeviceMatrix(intProxy,1);
}

template<typename T>
__global__ void setDeviceConstant(T* data,const int nRow, const int nCol,  const T number){
    int tid = threadIdx.x;
    int offset = nRow*blockIdx.x;

    while (tid < blockDim.x){
        data[tid  + offset] = number;
        tid += blockDim.x;
    }
}

__global__ void TestKernel(CudaMatrix_<double>* T){
    printf("This should be %G \n", T->devptr[0]);
    T->devptr[0]=1.0;
}

void test_function(){
    CudaMatrix_<double> mydouble(10,10);
    CudaMatrix_<double> *devMatrix;
    const size_t size = sizeof(CudaMatrix_<double>);
    cudaMalloc((void**)&devMatrix, size);
    cudaMemcpy(devMatrix, &mydouble, size, cudaMemcpyHostToDevice);
    TestKernel<<<1,1>>>(devMatrix);
    mydouble.syncDeviceToHost();
    std::cout << "Moment of Truth" << std::endl;
    mydouble.printMatrix();
    cudaFree(devMatrix);
}
template<typename T>
CUDA_HOST void CudaMatrixhelper_<T>::setDeviceMatrix(CudaMatrix_<T> &other, const T value) {
    assert(other.getNCol()<1024);
    setDeviceConstant<<<512, other.getNCol()>>>(other.devptr, other.getNRow(), other.getNCol(), value);
}

template class CudaMatrix_<double>;
template class CudaMatrix_<int>;
template class CudaMatrix_<float>;

template class CudaMatrixhelper_<double>;
template class CudaMatrixhelper_<float>;
template class CudaMatrixhelper_<int>;