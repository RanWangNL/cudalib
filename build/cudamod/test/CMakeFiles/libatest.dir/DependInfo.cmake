# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp" "/home/user/Desktop/CudaTrial/build/cudamod/test/CMakeFiles/libatest.dir/sampletest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/cuda-7.5/include"
  "/home/user/Desktop/CudaTrial/src/cudamod/test/lib/googletest/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
