# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/user/Desktop/CudaTrial/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/user/Desktop/CudaTrial/build

# Include any dependencies generated for this target.
include cudamod/CMakeFiles/libCudaMatrixtest.dir/depend.make

# Include the progress variables for this target.
include cudamod/CMakeFiles/libCudaMatrixtest.dir/progress.make

# Include the compile flags for this target's objects.
include cudamod/CMakeFiles/libCudaMatrixtest.dir/flags.make

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o: cudamod/CMakeFiles/libCudaMatrixtest.dir/flags.make
cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o: /home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/user/Desktop/CudaTrial/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o"
	cd /home/user/Desktop/CudaTrial/build/cudamod && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o -c /home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.i"
	cd /home/user/Desktop/CudaTrial/build/cudamod && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp > CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.i

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.s"
	cd /home/user/Desktop/CudaTrial/build/cudamod && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp -o CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.s

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.requires:
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.requires

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.provides: cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.requires
	$(MAKE) -f cudamod/CMakeFiles/libCudaMatrixtest.dir/build.make cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.provides.build
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.provides

cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.provides.build: cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o

# Object files for target libCudaMatrixtest
libCudaMatrixtest_OBJECTS = \
"CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o"

# External object files for target libCudaMatrixtest
libCudaMatrixtest_EXTERNAL_OBJECTS =

cudamod/libCudaMatrixtest: cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o
cudamod/libCudaMatrixtest: cudamod/CMakeFiles/libCudaMatrixtest.dir/build.make
cudamod/libCudaMatrixtest: cudamod/test/lib/googletest/libgtest_main.so
cudamod/libCudaMatrixtest: cudamod/libcudamod.so
cudamod/libCudaMatrixtest: cudamod/test/lib/googletest/libgtest.so
cudamod/libCudaMatrixtest: /usr/local/cuda-7.5/lib64/libcudart.so
cudamod/libCudaMatrixtest: cudamod/CMakeFiles/libCudaMatrixtest.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable libCudaMatrixtest"
	cd /home/user/Desktop/CudaTrial/build/cudamod && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/libCudaMatrixtest.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
cudamod/CMakeFiles/libCudaMatrixtest.dir/build: cudamod/libCudaMatrixtest
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/build

cudamod/CMakeFiles/libCudaMatrixtest.dir/requires: cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o.requires
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/requires

cudamod/CMakeFiles/libCudaMatrixtest.dir/clean:
	cd /home/user/Desktop/CudaTrial/build/cudamod && $(CMAKE_COMMAND) -P CMakeFiles/libCudaMatrixtest.dir/cmake_clean.cmake
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/clean

cudamod/CMakeFiles/libCudaMatrixtest.dir/depend:
	cd /home/user/Desktop/CudaTrial/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/user/Desktop/CudaTrial/src /home/user/Desktop/CudaTrial/src/cudamod /home/user/Desktop/CudaTrial/build /home/user/Desktop/CudaTrial/build/cudamod /home/user/Desktop/CudaTrial/build/cudamod/CMakeFiles/libCudaMatrixtest.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : cudamod/CMakeFiles/libCudaMatrixtest.dir/depend

