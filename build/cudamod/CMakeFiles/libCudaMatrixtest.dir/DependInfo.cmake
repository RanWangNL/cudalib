# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/user/Desktop/CudaTrial/src/cudamod/test/sampletest.cpp" "/home/user/Desktop/CudaTrial/build/cudamod/CMakeFiles/libCudaMatrixtest.dir/test/sampletest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/user/Desktop/CudaTrial/build/cudamod/CMakeFiles/cudamod.dir/DependInfo.cmake"
  "/home/user/Desktop/CudaTrial/build/cudamod/test/lib/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/user/Desktop/CudaTrial/src/cudamod/include/cudamod"
  "/usr/local/cuda-7.5/include"
  "/home/user/Desktop/CudaTrial/src/cudamod/test/lib/googletest/include"
  "/home/user/Desktop/CudaTrial/src/cudamod/include"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
