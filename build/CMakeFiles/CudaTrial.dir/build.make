# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/user/Desktop/CudaTrial/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/user/Desktop/CudaTrial/build

# Include any dependencies generated for this target.
include CMakeFiles/CudaTrial.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/CudaTrial.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/CudaTrial.dir/flags.make

CMakeFiles/CudaTrial.dir/main.cpp.o: CMakeFiles/CudaTrial.dir/flags.make
CMakeFiles/CudaTrial.dir/main.cpp.o: /home/user/Desktop/CudaTrial/src/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/user/Desktop/CudaTrial/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/CudaTrial.dir/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/CudaTrial.dir/main.cpp.o -c /home/user/Desktop/CudaTrial/src/main.cpp

CMakeFiles/CudaTrial.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/CudaTrial.dir/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/user/Desktop/CudaTrial/src/main.cpp > CMakeFiles/CudaTrial.dir/main.cpp.i

CMakeFiles/CudaTrial.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/CudaTrial.dir/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/user/Desktop/CudaTrial/src/main.cpp -o CMakeFiles/CudaTrial.dir/main.cpp.s

CMakeFiles/CudaTrial.dir/main.cpp.o.requires:
.PHONY : CMakeFiles/CudaTrial.dir/main.cpp.o.requires

CMakeFiles/CudaTrial.dir/main.cpp.o.provides: CMakeFiles/CudaTrial.dir/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/CudaTrial.dir/build.make CMakeFiles/CudaTrial.dir/main.cpp.o.provides.build
.PHONY : CMakeFiles/CudaTrial.dir/main.cpp.o.provides

CMakeFiles/CudaTrial.dir/main.cpp.o.provides.build: CMakeFiles/CudaTrial.dir/main.cpp.o

# Object files for target CudaTrial
CudaTrial_OBJECTS = \
"CMakeFiles/CudaTrial.dir/main.cpp.o"

# External object files for target CudaTrial
CudaTrial_EXTERNAL_OBJECTS =

CudaTrial: CMakeFiles/CudaTrial.dir/main.cpp.o
CudaTrial: CMakeFiles/CudaTrial.dir/build.make
CudaTrial: cudamod/libcudamod.so
CudaTrial: /usr/local/cuda-7.5/lib64/libcudart.so
CudaTrial: CMakeFiles/CudaTrial.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable CudaTrial"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/CudaTrial.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/CudaTrial.dir/build: CudaTrial
.PHONY : CMakeFiles/CudaTrial.dir/build

CMakeFiles/CudaTrial.dir/requires: CMakeFiles/CudaTrial.dir/main.cpp.o.requires
.PHONY : CMakeFiles/CudaTrial.dir/requires

CMakeFiles/CudaTrial.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/CudaTrial.dir/cmake_clean.cmake
.PHONY : CMakeFiles/CudaTrial.dir/clean

CMakeFiles/CudaTrial.dir/depend:
	cd /home/user/Desktop/CudaTrial/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/user/Desktop/CudaTrial/src /home/user/Desktop/CudaTrial/src /home/user/Desktop/CudaTrial/build /home/user/Desktop/CudaTrial/build /home/user/Desktop/CudaTrial/build/CMakeFiles/CudaTrial.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/CudaTrial.dir/depend

