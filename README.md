# README #

This is a user-developed cuda library.

To run this library, user needs to have a cuda enabled library. Furthermore, it should be of computational capability >= 2

# Current Features #

1. User Defined Matrix Class 
2. A helper class for several things
3. A reduce template (not implemented yet)

